package com.khaitran.shared.storage

import platform.Foundation.NSUserDefaults


class PlatformSettings: Settings {
    private val delegate: NSUserDefaults = NSUserDefaults.standardUserDefaults()

    override fun putBoolean(key: String, value: Boolean) {
        delegate.setBool(value, key)
    }

    override fun getBoolean(key: String, defaultValue: Boolean): Boolean =
            if (hasKey(key)) delegate.boolForKey(key) else defaultValue

    override fun putInt(key: String, value: Int) {
        delegate.setInteger(value.toLong(), key)
    }

    override fun getInt(key: String, defaultValue: Int): Int =
            if (hasKey(key)) delegate.integerForKey(key).toInt() else defaultValue

    override fun putString(key: String, value: String) {
        delegate.setObject(value, key)
    }

    override fun getString(key: String, defaultValue: String): String =
            if (hasKey(key)) delegate.stringForKey(key).toString() else defaultValue

    private fun hasKey(key: String): Boolean = delegate.objectForKey(key) != null
}