package com.khaitran.shared.storage

interface Settings {
    fun putBoolean(key: String, value: Boolean)
    fun getBoolean(key: String, defaultValue: Boolean = false): Boolean
    fun putInt(key: String, value: Int)
    fun getInt(key: String, defaultValue: Int = 0) : Int
    fun putString(key: String, value: String)
    fun getString(key: String, defaultValue: String = ""): String
}