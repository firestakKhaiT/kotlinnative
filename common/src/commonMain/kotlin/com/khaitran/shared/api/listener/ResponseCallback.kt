package com.khaitran.shared.api.listener

interface ResponseCallback<D, E> {
    fun onResult(response: D)
    fun onError(throwable: E)
}