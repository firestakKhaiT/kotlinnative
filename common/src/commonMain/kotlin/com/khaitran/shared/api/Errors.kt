package com.khaitran.shared.api

import io.ktor.client.response.HttpResponse

class ApiException(response: HttpResponse) : Throwable(response.status.toString())
class Unauthorized : Throwable()
class CannotConnectServer : Throwable()
class TooLongTimeResponse : Throwable()
class CannotConvertResponseData : Throwable()