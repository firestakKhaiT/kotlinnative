package com.khaitran.shared.api

import com.khaitran.shared.data.AllData
import com.khaitran.shared.data.Category
import com.khaitran.shared.data.CategoryItem
import io.ktor.client.HttpClient
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import io.ktor.client.response.HttpResponse
import io.ktor.http.*
import kotlinx.io.core.use
import kotlinx.serialization.serializer

class KtorApi(private val endPoint: String) {
    val client by lazy {
        HttpClient {
            install(Logging) {
                logger = Logger.DEFAULT
                level = LogLevel.ALL
            }
            install(ExpectSuccess)
            install(JsonFeature) {
                serializer = KotlinxSerializer().apply {
//                    setMapper(CategoryItem::class, CategoryItem.serializer())
                    register(String.serializer())
                    register(AllData.serializer())
                }
            }
        }
    }

    suspend fun isSuccess(type: HttpMethod, path: String, data: Any? = null): Boolean = client.request<HttpResponse> {
        apiUrl(path)
        method = type
        data?.let {
            json()
            body = data
        }
    }.use {
        it.status.isSuccess()
    }

    suspend inline fun <reified T> getAPI(path: String, data: Any? = null): T = client.get<T> {
        apiUrl(path)
        data?.let {
            body = data
        }
    }


    suspend inline fun <reified T> postAPI(path: String, data: Any?): T = client.post<T> {
        apiUrl(path)
        data?.let {
            json()
            body = data
        }
    }

    fun HttpRequestBuilder.json() {
        contentType(ContentType.Application.Json)
    }

    fun HttpRequestBuilder.apiUrl(path: String, token: String? = null) {
        if (token != null) {
            header(HttpHeaders.Authorization, "Bearer $token")
        }
        header(HttpHeaders.CacheControl, "no-cache")
        url {
            takeFrom(endPoint)
            encodedPath = path
        }
    }
}