package com.khaitran.shared.model

import com.khaitran.shared.api.KtorApi
import com.khaitran.shared.data.Category
import kotlinx.serialization.serializer

class ApiService {

    companion object {
        const val BASE_URL = "https://api.kotlinconf.com"
    }

    val apiService: KtorApi by lazy {
        KtorApi(BASE_URL)
    }

    suspend inline fun <reified T> get(path: String): T = apiService.getAPI(path)

    suspend inline fun <reified T> post(path: String, data: Any? = null): T = apiService.postAPI(path, data)

}