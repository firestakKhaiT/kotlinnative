package com.khaitran.shared.model

import com.khaitran.shared.storage.Settings
import kotlinx.coroutines.*
import kotlin.coroutines.*
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.JSON
import kotlin.properties.Delegates.observable
import kotlin.properties.ReadWriteProperty

abstract class DataRepository(val mainContext: CoroutineContext, val settings: Settings? = null): CoroutineScope {
    val api = ApiService()
    private val job = Job()
    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->

    }

    override val coroutineContext: CoroutineContext
        get() = mainContext + job + exceptionHandler

    open fun onDestroy() {
        job.cancel()
    }

    protected inline fun <reified T> getData(
            // Path of API
            path: String,
            // Complete
            crossinline onComplete: (T?, Throwable?) -> Unit,
            // Serializer of model
            serializer: KSerializer<T>? = null,
            // Using Cache
            key: String? = null,
            // From Cache
            fromCache: Boolean = false
    ) {
        val context = mainContext
        GlobalScope.launch(context) {
            try {
                // From local
                if (!key.isNullOrEmpty() && fromCache) {
                    val result = read(key!!, serializer)
                    result?.let {
                        onComplete(result, null)
                    }
                }

                // From server
                let {
                    var result = api.get<T>(path)
                    result?.let {
                        if (key.isNullOrEmpty()) {
                            // Call callback.onResult
                            onComplete(result, null)
                        } else {
                            val oldData = read(key!!, serializer)
                            if (result != oldData) {
                                // Call callback.onResult
                                onComplete(result, null)
                                // Save data on Local
                                write(key!!, result, serializer)
                            } else {
                                if (fromCache) {
                                } else {
                                    // Call callback.onResult
                                    onComplete(result, null)
                                }
                            }
                        }
                    }
                }
            } catch (error: Throwable) {
                onComplete(null, error)
            }
        }
    }

    /*
     * Local storage
     */

    inline fun <reified T : Any?> read(key: String, elementSerializer: KSerializer<T>?) =
            elementSerializer?.let {
                settings
                        ?.getString(key, "")
                        ?.takeUnless { it.isBlank() }
                        ?.let {
                            try {
                                JSON.parse(elementSerializer, it)
                            } catch (_: Throwable) {
                                null
                            }
                        }
            }

    inline fun <reified T : Any?> write(key: String, obj: Any?, elementSerializer: KSerializer<T>?) {
        elementSerializer?.let {
            settings?.putString(key, if (obj == null) "" else JSON.stringify(elementSerializer, obj as T))
        }
    }

    inline fun <reified T : Any?> bindToPreferencesByKey(
            key: String,
            elementSerializer: KSerializer<T>
    ): ReadWriteProperty<Any?, T?> = observable(read(key, elementSerializer)) { _, _, new ->
        write(key, new, elementSerializer)
    }
}