package com.khaitran.shared.model

import com.khaitran.shared.data.AllData
import com.khaitran.shared.storage.Settings
import kotlin.coroutines.CoroutineContext

class PlatformDataRepository(mainContext: CoroutineContext, settings: Settings) : DataRepository(mainContext, settings) {

    fun getAllCategory(onComplete: (AllData?, Throwable?) -> Unit, fromCache: Boolean = true) {
        getData("all", onComplete, AllData.serializer(), "listCategory", fromCache)
    }

    fun getName(): String{
        return "OK"
    }

}