package com.khaitran.shared.data

import kotlinx.serialization.Optional
import kotlinx.serialization.Serializable

//@Serializable
//data class Category(
//        val status: Int,
//        val result: String,
//        @Optional
//        val categories: List<CategoryItem> = emptyList()
//)
//
//@Serializable
//data class CategoryItem(
//        @Optional
//        val id: String? = null,
//        @Optional
//        val name: String? = null,
//        @Optional
//        val url_title: String? = null,
//        @Optional
//        val description: String? = null,
//        @Optional
//        val blurb: String? = null,
//        @Optional
//        val featured_image: String? = null,
//        @Optional
//        val thumb_image: String? = null,
//        @Optional
//        val parent_id: String? = null,
//        @Optional
//        val meta_description: String? = null,
//        @Optional
//        val meta_keywords: String? = null,
//        @Optional
//        val meta_title: String? = null,
//        @Optional
//        val sort_order: String? = null,
//        @Optional
//        val dropdown: Dropdown? = null
//)
//
//@Serializable
//data class Dropdown(
//        @Optional
//        val so: String? = null,
//        @Optional
//        val wn: String? = null,
//        @Optional
//        val blind: String? = null
//)