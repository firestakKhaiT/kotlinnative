package com.khaitran.shared.data

import kotlinx.serialization.*

@Serializable
data class Favorite(
    var sessionId: String
)