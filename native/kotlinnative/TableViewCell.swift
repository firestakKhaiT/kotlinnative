//
//  TableViewCell.swift
//  kotlinnative
//
//  Created by Tran Dinh Tuan Khai on 12/27/18.
//  Copyright © 2018 Tran Dinh Tuan Khai. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtSpeaker: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
