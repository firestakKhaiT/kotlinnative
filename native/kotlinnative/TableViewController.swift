//
//  TableViewController.swift
//  kotlinnative
//
//  Created by Tran Dinh Tuan Khai on 12/26/18.
//  Copyright © 2018 Tran Dinh Tuan Khai. All rights reserved.
//

import UIKit
import common

class TableViewController: UITableViewController {
    let dataRepository: PlatformDataRepository = PlatformDataRepository(mainContext: UI() as KotlinCoroutineContext, settings: PlatformSettings())
    let refreshController = UIRefreshControl()
    var speakers = [Speaker]()

    override func viewDidLoad() {
        super.viewDidLoad()
        refreshController.addTarget(self, action: #selector(updateData), for: .valueChanged)
        tableView.refreshControl = refreshController
        refreshController.beginRefreshing()
        dataRepository.getAllCategory(onComplete: { (response, error) -> KotlinUnit in
            if response != nil {
                self.speakers = response?.speakers ?? [Speaker]()
                self.tableView.reloadData()
                self.refreshController.endRefreshing()
            }
            return KotlinUnit()
        }, fromCache: true)
    }
    
    @objc func updateData() {
        dataRepository.getAllCategory(onComplete: { (response, error) -> KotlinUnit in
            if response != nil {
                self.speakers = response?.speakers ?? [Speaker]()
                self.tableView.reloadData()
                self.refreshController.endRefreshing()
            }
            return KotlinUnit()
        }, fromCache: false)
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return speakers.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell
        let item = speakers[indexPath.row]
        cell.txtName.text = item.fullName
        cell.txtSpeaker.text = item.tagLine
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
