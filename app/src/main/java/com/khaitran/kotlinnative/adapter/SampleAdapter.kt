package com.khaitran.kotlinnative.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.bumptech.glide.Glide
import com.khaitran.kotlinnative.R
import com.khaitran.shared.data.Speaker
import kotlinx.android.synthetic.main.item_list.view.*

class SampleAdapter(
        internal var arrItems: List<Speaker>
) : RecyclerView.Adapter<SampleAdapter.ViewHolder>() {
    private var lastPosition = -1

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): SampleAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.item_list, viewGroup, false))
    }

    override fun onBindViewHolder(viewHolder: SampleAdapter.ViewHolder, i: Int) {
        viewHolder.bind(arrItems[i])
//        startAnimation(viewHolder.itemView, i)
    }

    private fun startAnimation(view: View, pos: Int) {
        if (pos > lastPosition) {
            val animation = AnimationUtils.loadAnimation(view.context, R.anim.item_animation)
            view.startAnimation(animation)
            lastPosition = pos
        }
    }

    override fun getItemCount(): Int = arrItems.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Speaker) = with(itemView) {
            txtName.text = item.fullName
            txtDesc.text = item.tagLine
            Glide.with(imageView.context).load(R.drawable.icon_seven).into(imageView)
        }
    }

}
