package com.khaitran.kotlinnative.adapters.decorations

import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.khaitran.kotlinnative.adapter.SampleAdapter

class ItemDecoration(private val mDivider: Drawable, private val height: Int) : RecyclerView.ItemDecoration() {

    var marginLeft: Int = 0
    var marginRight: Int = 0

    constructor(mDivider: Drawable, height: Int, marginLeft: Int, marginRight: Int) : this(mDivider, height) {
        this.marginLeft = marginLeft
        this.marginRight = marginRight
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        if (mDivider == null) {
            if (parent.getChildAdapterPosition(view) != 0) {
                outRect.top = height
            }
        } else {
            if (parent.getChildAdapterPosition(view) != 0) {
                if (getOrientation(parent) == LinearLayoutManager.VERTICAL) {
                    outRect.top = mDivider.getIntrinsicHeight();
                } else {
                    outRect.left = mDivider.getIntrinsicWidth();
                }
            }
        }
    }

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        if (mDivider == null) {
            super.onDrawOver(c, parent, state)
            return
        }

        var left = 0
        var right = 0
        var top = 0
        var bottom = 0
        val size: Int
        val orientation = getOrientation(parent)
        var childCount = parent.childCount

        // using for loadmore
        (parent.adapter as? SampleAdapter)?.let {
            if (it.arrItems.last() == null) childCount--
        }

        if (orientation == LinearLayoutManager.VERTICAL) {
            size = mDivider.intrinsicHeight
            left = parent.paddingLeft + marginLeft
            right = parent.width - marginRight
        } else {    //horizontal
            size = mDivider.intrinsicWidth
            top = parent.paddingTop + marginLeft
            bottom = parent.height - marginRight
        }

        for (i in 1 until childCount) {
            val child = parent.getChildAt(i)

            if (orientation == LinearLayoutManager.VERTICAL) {
                top = child.top - size
                bottom = top + size
            } else {    //horizontal
                left = child.left - size
                right = left + size
            }
            mDivider.setBounds(left, top, right, bottom)
            mDivider.draw(c)
        }
    }

    private fun getOrientation(parent: RecyclerView): Int {
        if (parent.layoutManager is LinearLayoutManager) {
            val layoutManager = parent.layoutManager as LinearLayoutManager?
            return layoutManager!!.orientation
        } else {
            throw IllegalStateException(
                    "DividerItemDecoration can only be used with a LinearLayoutManager.")
        }
    }

}
