package com.khaitran.kotlinnative.ui.main

import android.util.Log
import com.khaitran.kotlinnative.ui.base.BasePresenter
import com.khaitran.shared.api.listener.ResponseCallback
import com.khaitran.shared.data.AllData
import com.khaitran.shared.model.PlatformDataRepository

class RecyclerViewPresenter<V : RecyclerViewView?>(override var view: V?, val dataRepository: PlatformDataRepository) : BasePresenter<V> {

    fun loadData(fromCache: Boolean = true) {
        dataRepository.getAllCategory(onComplete = { response, error ->
            response?.let {
                Log.e("KotlinNative", Thread.currentThread().name)
                view?.showData(response.speakers)
                view?.setRefreshingStatus(false)
            }
            error?.let{
                view?.showToast(error.localizedMessage ?: "")
            }
        }, fromCache = fromCache)
    }

    fun refreshData() {
        loadData(false)
    }
}