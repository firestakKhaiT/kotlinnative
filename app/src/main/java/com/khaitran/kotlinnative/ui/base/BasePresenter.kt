package com.khaitran.kotlinnative.ui.base

interface BasePresenter<V: BaseView?>: Presenter<V>{
    var view: V?

    override fun attachView(view: V) {
        this.view = view
    }

    override fun detachView() {
        view = null
    }
}