package com.khaitran.kotlinnative.ui.base

interface Presenter<V> {
    fun attachView(view: V)
    fun detachView()
}