package com.khaitran.kotlinnative.ui.main

import com.khaitran.kotlinnative.ui.base.BaseView
import com.khaitran.shared.data.Speaker

interface RecyclerViewView: BaseView{
    fun showData(data: List<Speaker>)
    fun setRefreshingStatus(isRefreshing: Boolean)
    fun showToast(message: String)
}