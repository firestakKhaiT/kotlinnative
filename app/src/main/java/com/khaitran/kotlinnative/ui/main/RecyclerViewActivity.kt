package com.khaitran.kotlinnative.ui.main

import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.khaitran.kotlinnative.R
import com.khaitran.kotlinnative.adapter.SampleAdapter
import com.khaitran.kotlinnative.adapters.decorations.ItemDecoration
import com.khaitran.kotlinnative.common.Utils
import com.khaitran.kotlinnative.storage.PlatformSettings
import com.khaitran.shared.data.Speaker
import com.khaitran.shared.model.PlatformDataRepository
import kotlinx.android.synthetic.main.activity_recyclerview.*
import kotlinx.coroutines.Dispatchers

class RecyclerViewActivity : AppCompatActivity(), RecyclerViewView {
    private lateinit var dataRepository: PlatformDataRepository
    private lateinit var presenter: RecyclerViewPresenter<RecyclerViewActivity>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataRepository = PlatformDataRepository(Dispatchers.Main, PlatformSettings(this@RecyclerViewActivity))
        presenter = RecyclerViewPresenter<RecyclerViewActivity>(this, dataRepository)
        setContentView(R.layout.activity_recyclerview)

        presenter.attachView(this)
        initViews()
        initEvents()

        presenter.loadData()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    private fun initViews() {
        rvList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvList.addItemDecoration(ItemDecoration(
                ResourcesCompat.getDrawable(resources, R.drawable.item_divider, null)!!,
                height = Utils.convertDpToPixel(0.5f, this),
                marginLeft = Utils.convertDpToPixel(100f, this),
                marginRight = Utils.convertDpToPixel(20f, this)))
    }

    private fun initEvents() {
        swipeLayout.setOnRefreshListener {
            presenter.refreshData()
        }
    }

    override fun showData(data: List<Speaker>) {
        rvList.adapter = SampleAdapter(data)
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun setRefreshingStatus(isRefreshing: Boolean) {
        swipeLayout.isRefreshing = isRefreshing
    }

    override fun showLoading() {
    }

    override fun hideLoading() {
    }

}
