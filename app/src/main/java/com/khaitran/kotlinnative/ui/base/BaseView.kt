package com.khaitran.kotlinnative.ui.base

interface BaseView {
    fun showLoading()
    fun hideLoading()
}