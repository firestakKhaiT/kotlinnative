package com.khaitran.kotlinnative.common

import android.content.Context
import com.khaitran.kotlinnative.R

object Utils {

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    fun convertDpToPixel(dp: Float, context: Context): Int {
        return (dp * context.resources.displayMetrics.density).toInt()
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    fun convertPixelsToDp(px: Float, context: Context): Int {
        return (px / context.resources.displayMetrics.density).toInt()
    }

    fun getIconDrawable(pos: Int): Int {
        when (pos) {
            1 -> return R.drawable.icon_one
            2 -> return R.drawable.icon_two
            3 -> return R.drawable.icon_three
            4 -> return R.drawable.icon_four
            5 -> return R.drawable.icon_five
            6 -> return R.drawable.icon_six
            7 -> return R.drawable.icon_seven
            else -> return R.drawable.icon_eight
        }
    }

}
