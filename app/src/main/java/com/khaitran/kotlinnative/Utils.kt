package com.khaitran.kotlinnative

import android.content.Context
import android.util.Log

fun Context.error(message: String?) {
    Log.e("KotlinNative", message)
}

fun Context.info(message: String?) {
    Log.i("KotlinNative", message)
}