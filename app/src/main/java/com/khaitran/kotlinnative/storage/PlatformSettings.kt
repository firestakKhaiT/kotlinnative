package com.khaitran.kotlinnative.storage

import android.content.Context
import android.preference.PreferenceManager
import com.khaitran.shared.storage.Settings

class PlatformSettings(context: Context) : Settings {
    private val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.applicationContext)

    override fun getBoolean(key: String, defaultValue: Boolean): Boolean =
            sharedPreferences.getBoolean(key, defaultValue)

    override fun getInt(key: String, defaultValue: Int): Int =
            sharedPreferences.getInt(key, defaultValue)

    override fun getString(key: String, defaultValue: String): String =
            sharedPreferences.getString(key, defaultValue) ?: defaultValue

    override fun putBoolean(key: String, value: Boolean) {
        sharedPreferences.edit().putBoolean(key, value).apply()
    }

    override fun putInt(key: String, value: Int) {
        sharedPreferences.edit().putInt(key, value).apply()
    }

    override fun putString(key: String, value: String) {
        sharedPreferences.edit().putString(key, value).apply()
    }
}